### Shrink Disorder Tarot / Card Game
###### The problem with normal Tarot or other Card Games is, that it (intentionally) lacks Technology, Method and Algorithm Cards.

# Outdated
## NEW REPO: https://github.com/braindef/Shrink-Disorder-Tarot-0.2.0
## NEW GAME: https://github.com/braindef/Hackers-Cardgame
(not yet populated with cards)

 - [ movie- | game- | jungian-] - archetypes (normal tarot)
 - [ movie- | classical- ] - psychiatrists
 - brain area
 - chemicals
 - algorithms / software
 - methods
 - technologies
 - symbolism

##### Pivot Elements:
 - ~1930: Donald Ewen Cameron && Eugen Bleuler @ Burghölzli Schweiz (UNI Zurich)
 - ~1950: CIA MKULTRA Projects 
 - ~1970: Explosion of the Numbers of Mental Deseases (DSM)


### QuickStart / HOWTO create a complete card deck:

- https://www.youtube.com/watch?v=L5aITiIZp9U (Just run the printAll.sh)
you can use GNU Debian Linux Jessie, you can boot it from a USB3.0 Stick, ask the local Nerd/Sheldon/Geek in your social network, he was trained to help you, WARNING, he will bite you if you are unfair to him :)

### QuickStart for creating your own cards:
* Download and install <a href="https://inkscape.org/de/">Inkscape</a>
* Download Template: <a href="https://github.com/braindef/Shrink-Disorder-Game/blob/master/HOWTO/InkscapeVorlageTarot.svg">https://github.com/braindef/Shrink-Disorder-Game/blob/master/HOWTO/InkscapeVorlageTarot.svg</a>
* Open the Template

<a href="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/HOWTO/HOWTO.png"><img src="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/HOWTO/HOWTO.png" width=400></a>

Short Video to show how to create a card, about 5 Minutes: 
https://www.youtube.com/watch?v=K0uYfV4gpes

(how to get the complete repository you can see in the Video "QuickStart")
* "git clone https://github.com/braindef/Shrink-Disorder-Tarot/"
* run the required script, normally you will just need the printAll.sh in the assemblies directory 

##### Game Idea:
**You've been owned** OR **You’ve been set free**

##### Alternative Use-Case:
https://twitter.com/FailDef/status/809218950420713472

##### Examples:
Spinal Cord Implant that was enhanced with all nerves **beats** the Stimoceiver or the WO2005055579A1 Patent, since the Spinal Cord Implant would have more influence to the body than the oder Technologies.

<img src="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/t6.png" width=200> <img src="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/t1.png" width=200> <img src="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/t3.png" width=200>

Or FARCRY-SkinnerBOX **beats** 1960s-SkinnerBOX

<img src="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/t8.png" width=200> <img src="http://psychiatrie-leaks.ch/Shrink-Disorder-Game/t2.png" width=200>


May this repository be under Creative Commons by-nc-sa and may these be broken, in case kids and jounger people can be protected by changing the License

![](https://raw.githubusercontent.com/braindef/Shrink-Disorder-Tarot/master/by-nc-sa.png)
https://creativecommons.org/licenses/by-nc-sa/4.0/

For the Patent-Trolls, i tried to use Images, Fonts... that are Creative Commons or free, if something went wrong and i unfortunately took something that is illegal, please tell me in "issues" so i can remove it...

Das Projekt ist für den Moment mal auf http://psychiatrie-leaks.ch/Shrink-Disorder-Tarot zuhause (mirror: http://marclandolt.ch/Shrink-Disorder-Tarot)
English Version on http://psychiatrie-leaks.ch/Shrink-Disorder-Tarot/index_EN.html (mirror: http://marclandolt.ch/Shrink-Disorder-Tarot/index_EN.html)


